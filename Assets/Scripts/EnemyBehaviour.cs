﻿using Unity.Entities;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class EnemyBehaviour : MonoBehaviour, IConvertGameObjectToEntity
{
    [SerializeField]
	private float speed = 2f;
    [SerializeField]
    private float enemyHealth = 1f;

	private Rigidbody rigidBody;


	private void Start()
	{
		rigidBody = GetComponent<Rigidbody>();
	}

	private void Update()
	{
		if (!Global.IsPlayerDead())
		{
			Vector3 heading = Global._pPlayerPosition - transform.position;
			heading.y = 0f;
			transform.rotation = Quaternion.LookRotation(heading);
		}

		Vector3 movement = transform.forward * speed * Time.deltaTime;
		rigidBody.MovePosition(transform.position + movement);
	}

	//Enemy Collision
	private void OnTriggerEnter(Collider theCollider)
	{

	}

	public void Convert(Entity entity, EntityManager manager, GameObjectConversionSystem conversionSystem)
	{
		manager.AddComponent(entity, typeof(EnemyTag));
		manager.AddComponent(entity, typeof(MoveForward));

		MoveSpeed moveSpeed = new MoveSpeed { Value = speed };
		manager.AddComponentData(entity, moveSpeed);

		Health health = new Health { Value = enemyHealth };
		manager.AddComponentData(entity, health);
	}
}
