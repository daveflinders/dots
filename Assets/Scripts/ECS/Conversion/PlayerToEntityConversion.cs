﻿using Unity.Entities;
using UnityEngine;

public class PlayerToEntityConversion : MonoBehaviour, IConvertGameObjectToEntity
{
    [SerializeField]
    private float _healthValue = 1f;


	public void Convert(Entity entity, EntityManager manager, GameObjectConversionSystem conversionSystem)
	{
		manager.AddComponent(entity, typeof(PlayerTag));

		Health health = new Health { Value = _healthValue };
		manager.AddComponentData(entity, health);
	}
}