﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;


[UpdateAfter( typeof( MoveForwardSystem ) )]
public class TimedDestroySystem : JobComponentSystem
{
    private EndSimulationEntityCommandBufferSystem _buffer;

    protected override void OnCreateManager()
    {
        _buffer = World.Active.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    struct CullingJob : IJobForEachWithEntity<TimeToLive>
    {
        public EntityCommandBuffer.Concurrent _commands;
        public float _dt;

        public void Execute( Entity entity, int jobIndex, ref TimeToLive timeToLive )
        {
            timeToLive.Value -= _dt;
            if( timeToLive.Value <= 0f )
                _commands.DestroyEntity( jobIndex, entity );
        }
    }

    protected override JobHandle OnUpdate( JobHandle inputDeps )
    {
        var job = new CullingJob
        {
            _commands = _buffer.CreateCommandBuffer().ToConcurrent(),
            _dt = Time.deltaTime
        };

        var handle = job.Schedule( this, inputDeps );
        _buffer.AddJobHandleForProducer( handle );

        return handle;
    }
}

