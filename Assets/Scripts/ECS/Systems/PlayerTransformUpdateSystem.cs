﻿using Unity.Entities;
using Unity.Transforms;

public class PlayerTransformUpdateSystem : ComponentSystem
{
    protected override void OnUpdate()
    {
        if( Global.IsPlayerDead() )
            return;

        Entities.WithAll<PlayerTag>().ForEach( ( ref Translation pos ) =>
         {
             pos = new Translation { Value = Global._pPlayerPosition };
         } );
    }
}