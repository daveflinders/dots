﻿using UnityEngine;

public class Global : MonoBehaviour
{
    private static Global _instance;

    [SerializeField]
    private Transform _player;
    [SerializeField]
    private float _playerCollisionRadius = .5f;
    public static float _pPlayerCollisionRadius
    {
        get { return _instance._playerCollisionRadius; }
    }

    public float _enemyCollisionRadius = .3f;
    public static float _pEnemyCollisionRadius
    {
        get { return _instance._enemyCollisionRadius; }
    }

    public static Vector3 _pPlayerPosition
    {
        get { return _instance._player.position; }
    }

    private void Awake()
    {
        if( _instance != null && _instance != this )
            Destroy( gameObject );
        else
            _instance = this;
    }

    public static Vector3 GetPositionAroundPlayer( float radius )
    {
        Vector3 playerPos = _instance._player.position;

        float angle = UnityEngine.Random.Range( 0f, 2 * Mathf.PI );
        float s = Mathf.Sin( angle );
        float c = Mathf.Cos( angle );

        return new Vector3( c * radius, 1.1f, s * radius ) + playerPos;
    }

    public static void PlayerDied()
    {
        if( _instance._player == null )
            return;

        Debug.Log( "Player DEAD! Do something..." );
        return;

        PlayerMovementAndLook playerMove = _instance._player.GetComponent<PlayerMovementAndLook>();
        playerMove.PlayerDied();

        _instance._player = null;
    }

    public static bool IsPlayerDead()
    {
        return _instance._player == null;
    }
}
