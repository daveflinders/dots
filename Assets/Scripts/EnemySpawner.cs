﻿using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private bool _spawnEnemies = true;
    [SerializeField]
    private bool _useECS = false;
    [SerializeField]
    private float _enemySpawnRadius = 10f;
    [SerializeField]
    private GameObject _enemyPrefab;

    [Range( 1, 100 )]
    [SerializeField]
    private int _spawnsPerInterval = 1;

    [Range( 0.1f, 2f )]
    [SerializeField]
    private float _spawnInterval = 1f;

    private EntityManager _manager;
    private Entity _enemyEntityPrefab;

    private float _cooldown;


    private void Start()
    {
        if( _useECS )
        {
            _manager = World.Active.EntityManager;
            _enemyEntityPrefab = GameObjectConversionUtility.ConvertGameObjectHierarchy( _enemyPrefab, World.Active );
        }
    }

    private void Update()
    {
        if( !_spawnEnemies || Global.IsPlayerDead() )
            return;

        _cooldown -= Time.deltaTime;

        if( _cooldown <= 0f )
        {
            _cooldown += _spawnInterval;
            Spawn();
        }
    }

    private void Spawn()
    {
        for( int i = 0; i < _spawnsPerInterval; i++ )
        {
            Vector3 pos = Global.GetPositionAroundPlayer( _enemySpawnRadius );

            if( !_useECS )
            {
                Instantiate( _enemyPrefab, pos, Quaternion.identity );
            }
            else
            {
                Entity enemy = _manager.Instantiate( _enemyEntityPrefab );
                _manager.SetComponentData( enemy, new Translation { Value = pos } );
            }
        }
    }
}
