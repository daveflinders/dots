﻿using UnityEngine;

public class PlayerMovementAndLook : MonoBehaviour
{
    [SerializeField]
    private Camera _mainCamera;
    [SerializeField]
    private float _speed = 4.5f;
    [SerializeField]
    private LayerMask _whatIsGround;
    [SerializeField]
    private float _playerHealth = 1f;

    private Rigidbody _playerRigidbody;
    private bool _isDead;

    private void Awake()
    {
        _playerRigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if( _isDead )
            return;

        //Arrow Key Input
        float h = Input.GetAxis( "Horizontal" );
        float v = Input.GetAxis( "Vertical" );

        Vector3 inputDirection = new Vector3( h, 0, v );

        //Camera Direction
        var cameraForward = _mainCamera.transform.forward;
        var cameraRight = _mainCamera.transform.right;

        cameraForward.y = 0f;
        cameraRight.y = 0f;

        //Try not to use var for roadshows or learning code
        Vector3 desiredDirection = cameraForward * inputDirection.z + cameraRight * inputDirection.x;

        //Why not just pass the vector instead of breaking it up only to remake it on the other side?
        MoveThePlayer( desiredDirection );

    }

    void MoveThePlayer( Vector3 desiredDirection )
    {
        Vector3 movement = new Vector3( desiredDirection.x, 0f, desiredDirection.z );
        movement = movement.normalized * _speed * Time.deltaTime;

        _playerRigidbody.MovePosition( transform.position + movement );
    }

    //Player Collision
    void OnTriggerEnter( Collider theCollider )
    {
        if( !theCollider.CompareTag( "Enemy" ) )
            return;

        _playerHealth--;

        if( _playerHealth <= 0 )
        {
            Global.PlayerDied();
        }
    }

    public void PlayerDied()
    {
        if( _isDead )
            return;

        _isDead = true;
        _playerRigidbody.isKinematic = true;
        GetComponent<Collider>().enabled = false;
    }
}
